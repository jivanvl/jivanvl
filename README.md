# About myself

The idea of this README is to provide an image on who I am and how I work, this document will get updated every now and them

## Work

* I love working async, tagging me on issues, sending me emails, this helps me stay organized.
* I enjoy collaborating and build great relationships along the way, I have met some great friends in my career and I'd like to make more given the opportunity.
* Response SLA's: I try to be as diligent as possible when getting back to people, feel free to ping me if you feel that I haven't gotten back to you in a reasonable amount of time.
* I prefer the GTD (Gettings Things Done™) system, my favorite TODO's app is [Things](https://culturedcode.com/things/), but I also like any software that manages TODO's, like GitLab or Trello. They are my single source of truth for all of the tasks that I have to do in a work day.
* Outside of my working hours I walk away from work related devices to help with my work/life balance.

## Communication

* For code reviews I use [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0-beta.2/) to help provide context on my review comments.
* During meetings I like to take notes, so please understand that sometimes when I'm not looking at the screen during a meeting is that I take notes on either physical media or another device.
* Direct messages are fine, just bear in mind that I might not be able to respond immediately, I understand this also applies to the other person on the screen when I direct message someone.

## Mentorship

I became an open source maintainer trainer for the first time in 2023, this is a new experience but these are some of the ways that I work with trainees to help them become ready to maintain large codebases:

### 1:1's

* Since the experience is similar to that of a relationship between a manager and an individual contributor, a 1:1 document is created and a cadence set.
* A starting cadence is set and modified as the trainee becomes more comfortable tackling tasks and questions on their own, I'm always available for support for any outstanding topics beyond the time set for our 1:1's.
* During 1:1's, I prioritize my trainee's items first and move on to mine afterwards. Most of the time my items are related to interesting Merge Requests or Pull Requests that can show fascinating situations that could happen during a review.
* I'm flexible with 1:1's, we can always reschedule, cancel when they are not needed, etc.

### Homework

* I know homework is not fun, but I like to provide my trainees with [meta reviews](https://twitter.com/gergelyorosz/status/1619797377858297857?s=61&t=24zDcF16NYMjgnOjGidnEA), the idea is for them to review an already merged merge request or pull request and see what they did differently, what were the things that they shared with reviewers.
* Another bit of homework for trainees that I highly encourage to do, is to set a time to set update their "training issue" or document where they ask for maintainers for feedback on reviews that they have done recently to see if they're making progress or if they need to improve on a particular topic. Just like a brag doc, this document should provide evidence that you are ready to maintain a project.

## How you can help me

* I value feedback in any form, I'm working on a way to allow teammates to provide me with feedback anonymously.

## Relevant pages and socials

* [DevCafe MX](https://devcafe.mx)
* [LinkedIn](https://www.linkedin.com/in/jivanvl/)

## Personal

* I enjoy photography and I even have a Sony A7IV full frame camera! Some of the photos on [DevCafe MX](https://devcafe.mx) are my own.
* I'm an avid video game collector; I love the hobby, but it can be expensive.
